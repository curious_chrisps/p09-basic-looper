//
//  Looper.cpp
//  sdaLooper
//
//  Created by tj3-mitchell on 21/01/2013.
//
//

#include "Looper.h"

Looper::Looper() :  recordState (false),    //initialise - not recording
                    playState (false),      //not playing
                    bufferPosition (0)      //position to the start of audioSampleBuffer
{
    
    playState = false;
    recordState = false;
    bufferPosition = 0;
    
    //audioSampleBuffer contents to zero
//    for (int count = 0; count < bufferSize; count++)
//        audioSampleBuffer[count] = 0.f;
    audioSampleBuffer.setSize(1, 176400);
    audioSampleBuffer.clear();
}

Looper::~Looper()
{

}

void Looper::setPlayState (const bool newState)
{
    playState = newState;
}

bool Looper::getPlayState () const
{
    return playState.get();     //using .get() because it's an atomic variable
}

void Looper::setRecordState (const bool newState)
{
    recordState = newState;
}

bool Looper::getRecordState () const
{
    return recordState.get();   //using .get() because it's an atomic variable
}

float Looper::processSample (float input)
{
    float output = 0.f;
    if (playState.get() == true)
    {
        //play
        float* audioSample = audioSampleBuffer.getWritePointer(0, bufferPosition);
        output = *audioSample;
        //click 4 times each bufferLength
        if ((bufferPosition % (bufferSize / 8)) == 0)
            output += 0.25f;
        
        //record
        if (recordState.get() == true)
        {
            *audioSample += input;
            output += *audioSample; // Constantly adding - overdub
            //audioSampleBuffer[bufferPosition] = (audioSampleBuffer[bufferPosition]+input)/2.0; // Adding but making sure it won't get too loud or messy.
        }
        
        //increment and cycle the buffer counter
        ++bufferPosition;
        if (bufferPosition == bufferSize)
            bufferPosition = 0;
    }
    return output;
}

void Looper::save()
{
    FileChooser chooser ("Please select a file",
                         File::getSpecialLocation (File::userDesktopDirectory),
                         "*.wav");
    if (chooser.browseForFileToSave(true))
    {
        File file (chooser.getResult().withFileExtension (".wav"));
        OutputStream* outStream = file.createOutputStream();
        WavAudioFormat wavFormat;
        AudioFormatWriter* writer = wavFormat.createWriterFor(outStream, 44100, 1, 16, NULL, 0);
        writer->writeFromAudioSampleBuffer(audioSampleBuffer, 0, audioSampleBuffer.getNumSamples());
        delete writer;
    }
}

void Looper::load()
{
    AudioFormatManager formatManager;
    formatManager.registerBasicFormats();
    
    FileChooser chooser ("Please select a file to load...",
                         File::getSpecialLocation(File::userHomeDirectory),
                         formatManager.getWildcardForAllFormats());
    
    if (chooser.browseForFileToOpen())
    {
        File file (chooser.getResult());
        
        AudioFormatReader* reader = formatManager.createReaderFor (file);
        
        if (reader != 0)
        {
            audioSampleBuffer.setSize (reader->numChannels, (int)reader->lengthInSamples);
//            bufferSize = (int)reader->lengthInSamples; // This line assigns a new lenght to the buffer to accommodate the whole file
            reader->read(&audioSampleBuffer, 0, (int)reader->lengthInSamples, 0, true, false);
            delete reader;
        }
    }
}


