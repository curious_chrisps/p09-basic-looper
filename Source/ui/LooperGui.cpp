//
//  LooperGui.cpp
//  sdaLooper
//
//  Created by tj3-mitchell on 21/01/2013.
//
//

#include "LooperGui.h"

LooperGui::LooperGui(Looper& l) : looper (l)
{
    playButton.setButtonText ("Play");
    playButton.setConnectedEdges(Button::ConnectedOnLeft | Button::ConnectedOnRight);
    playButton.setColour(TextButton::buttonColourId, Colours::grey);
    playButton.setColour(TextButton::buttonOnColourId, Colours::lightgrey);
    addAndMakeVisible (&playButton);
    playButton.addListener (this);
    
    recordButton.setButtonText ("Record");
    recordButton.setConnectedEdges(Button::ConnectedOnLeft | Button::ConnectedOnRight);
    recordButton.setColour(TextButton::buttonColourId, Colours::darkred);
    recordButton.setColour(TextButton::buttonOnColourId, Colours::red);
    addAndMakeVisible (&recordButton);
    recordButton.addListener (this);
    
    saveButton.setButtonText("Save");
    saveButton.setConnectedEdges(Button::ConnectedOnLeft | Button::ConnectedOnRight);
    saveButton.setColour(TextButton::buttonColourId, Colours::lightseagreen);
    addAndMakeVisible(&saveButton);
    saveButton.addListener(this);
    
    loadButton.setButtonText("Load");
    loadButton.setConnectedEdges(Button::ConnectedOnLeft | Button::ConnectedOnRight);
    loadButton.setColour(TextButton::buttonColourId, Colours::darkgrey);
    addAndMakeVisible(&loadButton);
    loadButton.addListener(this);
}

LooperGui::~LooperGui()
{
    
}

void LooperGui::buttonClicked (Button* button)
{
    if (button == &playButton)
    {
        looper.setPlayState (!looper.getPlayState());
        playButton.setToggleState (looper.getPlayState(), dontSendNotification);
        if (looper.getPlayState())
            playButton.setButtonText ("Stop");
        else
            playButton.setButtonText ("Play");
    }
    else if (button == &recordButton)
    {
        looper.setRecordState (!looper.getRecordState());
        recordButton.setToggleState (looper.getRecordState(), dontSendNotification);

    }
    else if (button == &saveButton)
    {
        if(looper.getPlayState())
        {   AlertWindow::showMessageBoxAsync(AlertWindow::WarningIcon, "Error", "Stop playback before trying to save.", "OK", this);
        }
        else
        {
            looper.save();
        }
    }
    else if (button == &loadButton)
    {
        if(looper.getPlayState())
        {   AlertWindow::showMessageBoxAsync(AlertWindow::WarningIcon, "Error", "Stop playback before trying to load file.", "OK", this);
        }
        else
        {
            looper.load();
        }
    }
    
}

void LooperGui::resized()
{
    playButton.setBounds (0, 0, getWidth()/3, getHeight()/2);
    recordButton.setBounds (playButton.getBounds().translated(getWidth()/3, 0));
    saveButton.setBounds(recordButton.getX() + recordButton.getWidth(), 0, getWidth()/6, getHeight()/2);
    loadButton.setBounds(saveButton.getBounds().translated(getWidth()/6, 0));
}
